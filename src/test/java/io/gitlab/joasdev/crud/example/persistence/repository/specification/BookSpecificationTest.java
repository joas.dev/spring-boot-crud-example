package io.gitlab.joasdev.crud.example.persistence.repository.specification;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BookSpecificationTest {

  @Mock private Root<BookProjection> root;
  @Mock private CriteriaQuery<?> query;
  @Mock private CriteriaBuilder criteriaBuilder;

  @Test
  void shouldReturnNull() {

    BookSpecification bookSpecification = BookSpecification.builder().build();

    Predicate result = bookSpecification.toPredicate(root, query, criteriaBuilder);

    assertNull(result);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  void shouldReturnPredicateInOrValue() {

    BookSpecification bookSpecification = BookSpecification.builder().q("test").build();

    Path<String> titlePath = mock(Path.class);

    Predicate predicate = mock(Predicate.class);
    when(root.get("title")).thenReturn((Path) titlePath);

    when(criteriaBuilder.like(titlePath, "%test%")).thenReturn(predicate);

    when(criteriaBuilder.or(any(Predicate[].class))).thenReturn(predicate);

    Predicate result = bookSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(root, times(1)).get("title");
    verify(criteriaBuilder, times(1)).like(titlePath, "%test%");
    verify(criteriaBuilder, times(1)).or(any(Predicate[].class));
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  @Test
  void shouldReturnPredicateInAndValue() {

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Path<LocalDate> onlineAvailabilityPath = mock(Path.class);
    Path<Boolean> publicationDatePath = mock(Path.class);

    Predicate predicate = mock(Predicate.class);
    when(root.get("onlineAvailability")).thenReturn((Path) onlineAvailabilityPath);
    when(root.get("publicationDate")).thenReturn((Path) publicationDatePath);
    when(criteriaBuilder.equal(onlineAvailabilityPath, bookSpecification.getOnlineAvailability()))
        .thenReturn(predicate);
    when(criteriaBuilder.equal(publicationDatePath, bookSpecification.getPublicationDate()))
        .thenReturn(predicate);

    when(criteriaBuilder.and(any(Predicate[].class))).thenReturn(predicate);

    Predicate result = bookSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(criteriaBuilder, times(1))
        .equal(onlineAvailabilityPath, bookSpecification.getOnlineAvailability());
    verify(criteriaBuilder, times(1))
        .equal(publicationDatePath, bookSpecification.getPublicationDate());
    verify(criteriaBuilder, times(1)).and(any(Predicate[].class));
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  void shouldReturnPredicate() {

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Path<String> titlePath = mock(Path.class);
    Predicate titlePredicate = mock(Predicate.class);

    when(root.get("title")).thenReturn((Path) titlePath);
    when(criteriaBuilder.like(titlePath, "%" + bookSpecification.getQ() + "%"))
        .thenReturn(titlePredicate);

    Path<LocalDate> onlineAvailabilityPath = mock(Path.class);
    Path<Boolean> publicationDatePath = mock(Path.class);

    Predicate onlineAvailabilityPredicate = mock(Predicate.class);
    Predicate publicationDatePredicate = mock(Predicate.class);

    when(root.get("onlineAvailability")).thenReturn((Path) onlineAvailabilityPath);
    when(root.get("publicationDate")).thenReturn((Path) publicationDatePath);
    when(criteriaBuilder.equal(onlineAvailabilityPath, bookSpecification.getOnlineAvailability()))
        .thenReturn(onlineAvailabilityPredicate);
    when(criteriaBuilder.equal(publicationDatePath, bookSpecification.getPublicationDate()))
        .thenReturn(publicationDatePredicate);

    Predicate predicateAnd = mock(Predicate.class);
    Predicate predicateOr = mock(Predicate.class);
    Predicate predicate = mock(Predicate.class);

    when(criteriaBuilder.or(any(Predicate[].class))).thenReturn(predicateAnd);
    when(criteriaBuilder.and(any(Predicate[].class))).thenReturn(predicateOr);

    when(criteriaBuilder.and(any(Predicate.class), any(Predicate.class))).thenReturn(predicate);

    Predicate result = bookSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(criteriaBuilder, times(1))
        .equal(onlineAvailabilityPath, bookSpecification.getOnlineAvailability());
    verify(criteriaBuilder, times(1))
        .equal(publicationDatePath, bookSpecification.getPublicationDate());
  }
}
