package io.gitlab.joasdev.crud.example.persistence.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.dao.impl.BookDaoImpl;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.BookSpecification;
import io.gitlab.joasdev.crud.example.util.TestUtil;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

@ExtendWith(MockitoExtension.class)
class BookDaoTest {

  @InjectMocks private BookDaoImpl bookDao;

  @Mock private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  /*
   * @BeforeEach void setUp() { bookDao = spy(new BookDaoImpl(JdbcTemplate));
   * ReflectionTestUtils.setField(bookDao, "namedParameterJdbcTemplate",
   * namedParameterJdbcTemplate); }
   */

  @SuppressWarnings("serial")
  @Test
  void shouldReturnThrowInQueryForObjectMeyhod() {

    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenThrow(new DataAccessException("test") {});

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();

    assertThrows(ApiException.class, () -> bookDao.findAllToList(bookSpecification, sort));
    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
  }

  @SuppressWarnings("serial")
  @Test
  void shouldReturnThrowInFindAllToPageMeyhod() {
    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenThrow(new DataAccessException("test") {});

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();
    Pageable pageable = PageRequest.of(0, 10, sort);
    assertThrows(ApiException.class, () -> bookDao.findAllToPage(bookSpecification, pageable));

    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
  }

  @SuppressWarnings("unchecked")
  @Test
  void shouldReturnList() {

    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenReturn(1L);

    List<BookProjection> bookProjectionList = TestUtil.getBookProjectionList();
    when(namedParameterJdbcTemplate.query(
            anyString(), any(SqlParameterSource.class), any(RowMapper.class)))
        .thenReturn(bookProjectionList);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();

    List<BookProjection> result = bookDao.findAllToList(bookSpecification, sort);

    assertNotNull(result);

    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
    verify(namedParameterJdbcTemplate, times(1))
        .query(anyString(), any(SqlParameterSource.class), any(RowMapper.class));
  }

  @Test
  void shouldReturnListAndZeroValue() {

    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenReturn(0L);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();

    List<BookProjection> result = bookDao.findAllToList(bookSpecification, sort);

    assertNotNull(result);

    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
  }

  @SuppressWarnings("unchecked")
  @Test
  void shouldReturnPage() {

    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenReturn(1L);

    List<BookProjection> bookProjectionList = TestUtil.getBookProjectionList();

    when(namedParameterJdbcTemplate.query(
            anyString(), any(SqlParameterSource.class), any(RowMapper.class)))
        .thenReturn(bookProjectionList);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();
    Pageable pageable = PageRequest.of(0, 10, sort);

    Page<BookProjection> result = bookDao.findAllToPage(bookSpecification, pageable);

    assertNotNull(result);

    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
    verify(namedParameterJdbcTemplate, times(1))
        .query(anyString(), any(SqlParameterSource.class), any(RowMapper.class));
  }

  @Test
  void shouldReturnPageAndZeroValue() {

    when(namedParameterJdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), eq(Long.class)))
        .thenReturn(0L);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .q("test")
            .onlineAvailability(true)
            .publicationDate(LocalDate.now())
            .build();

    Sort sort = Sort.by("bookId").ascending();
    Pageable pageable = PageRequest.of(0, 10, sort);

    Page<BookProjection> result = bookDao.findAllToPage(bookSpecification, pageable);

    assertNotNull(result);

    verify(namedParameterJdbcTemplate, times(1))
        .queryForObject(anyString(), any(SqlParameterSource.class), eq(Long.class));
  }
}
