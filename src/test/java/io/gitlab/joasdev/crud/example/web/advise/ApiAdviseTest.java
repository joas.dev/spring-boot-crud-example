package io.gitlab.joasdev.crud.example.web.advise;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

@ExtendWith(MockitoExtension.class)
class ApiAdviseTest {

  @InjectMocks private ApiAdvise apiAdvise;

  @Test
  void shouldReturn() {
    BindingResult bindingResult = mock(BindingResult.class);

    List<ObjectError> errors = new ArrayList<>();
    errors.add(new FieldError("registerBookDto", "title", "Title is mandatory"));
    errors.add(new FieldError("registerBookDto", "onlineAvailability", "true"));

    when(bindingResult.getAllErrors()).thenReturn(errors);

    MethodArgumentNotValidException methodArgumentNotValidException =
        new MethodArgumentNotValidException(null, bindingResult);

    Map<String, String> result =
        apiAdvise.handleValidationExceptions(methodArgumentNotValidException);
    assertNotNull(result);
  }
}
