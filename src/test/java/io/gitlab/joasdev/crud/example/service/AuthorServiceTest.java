package io.gitlab.joasdev.crud.example.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.type.TypeReference;
import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.AuthorItemDto;
import io.gitlab.joasdev.crud.example.model.dto.AuthorResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import io.gitlab.joasdev.crud.example.persistence.repository.AuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.BookAuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.AuthorSpecification;
import io.gitlab.joasdev.crud.example.service.impl.AuthorServiceImpl;
import io.gitlab.joasdev.crud.example.util.TestUtil;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class AuthorServiceTest {

  @InjectMocks private AuthorServiceImpl authorServiceImpl;

  @Mock private AuthorRepository authorRepository;
  @Mock private BookAuthorRepository bookAuthorRepository;

  @Test
  void shouldDeletedAuthorAndDeletedBook() {

    when(bookAuthorRepository.existByAuthorId(anyInt())).thenReturn(true);
    when(bookAuthorRepository.deleteByAuthorId(anyInt())).thenReturn(1);
    doNothing().when(authorRepository).delete(anyInt());

    authorServiceImpl.deleteAuthor(anyInt());
    verify(bookAuthorRepository, times(1)).existByAuthorId(anyInt());
    verify(bookAuthorRepository, times(1)).deleteByAuthorId(anyInt());
  }

  @Test
  void shouldReturnAuthor() {
    AuthorProjection authorProjection = TestUtil.getAuthorProjection();
    when(authorRepository.findByUserId(anyInt())).thenReturn(authorProjection);

    AuthorResponseDto result = authorServiceImpl.findAuthorId(1);

    assertNotNull(result);
  }

  @Test
  void shouldReturnThrowInAuthor() {
    when(authorRepository.findByUserId(anyInt())).thenReturn(null);

    assertThrows(Exception.class, () -> authorServiceImpl.findAuthorId(1));
  }

  @Test
  void shouldReturnThrowInMethodFindAuthorId() {

    when(authorRepository.findByUserId(anyInt())).thenThrow(new RuntimeException("test"));

    assertThrows(Exception.class, () -> authorServiceImpl.findAuthorId(1));
  }

  @Test
  void shouldDeletedAuthor() {

    when(bookAuthorRepository.existByAuthorId(anyInt())).thenReturn(null);
    doNothing().when(authorRepository).delete(anyInt());

    authorServiceImpl.deleteAuthor(anyInt());
    verify(bookAuthorRepository, times(1)).existByAuthorId(anyInt());
  }

  @Test
  void shouldDeletedAuthorAndExistAuthorId() {

    when(bookAuthorRepository.existByAuthorId(anyInt())).thenReturn(true);
    doNothing().when(authorRepository).delete(anyInt());

    authorServiceImpl.deleteAuthor(anyInt());
    verify(bookAuthorRepository, times(1)).existByAuthorId(anyInt());
  }

  @Test
  void shouldReturnThrowInDeletedAuthor() {

    when(bookAuthorRepository.existByAuthorId(anyInt())).thenThrow(new RuntimeException("test"));

    assertThrows(ApiException.class, () -> authorServiceImpl.deleteAuthor(1));
  }

  @Test
  void shouldReturnPage() throws IOException {

    Author author =
        TestUtil.loadObjectFromResource("authors/entity/author_entity.json", Author.class);
    List<Author> authorList = Arrays.asList(author);
    Page<Author> authorPage = new PageImpl<>(authorList);
    when(authorRepository.findAll(any(AuthorSpecification.class), any(Pageable.class)))
        .thenReturn(authorPage);
    AuthorSpecification authorSpecification =
        AuthorSpecification.builder().birthdate(LocalDate.now()).q("test").build();

    Pageable pageable = PageRequest.of(0, 10);
    Page<AuthorItemDto> result = authorServiceImpl.findAllToPage(authorSpecification, pageable);
    assertNotNull(result);
  }

  @Test
  void shouldReturnThrowInPage() {

    when(authorRepository.findAll(any(AuthorSpecification.class), any(Pageable.class)))
        .thenThrow(new RuntimeException("test"));

    AuthorSpecification authorSpecification =
        AuthorSpecification.builder().birthdate(LocalDate.now()).q("test").build();

    Pageable pageable = PageRequest.of(0, 10);
    assertThrows(
        Exception.class, () -> authorServiceImpl.findAllToPage(authorSpecification, pageable));
  }

  @Test
  void shouldReturnList() {

    List<Author> authorList =
        TestUtil.loadObjectFromResource(
            "authors/response/author_list.json", new TypeReference<List<Author>>() {});

    when(authorRepository.findAll(any(AuthorSpecification.class))).thenReturn(authorList);
    AuthorSpecification authorSpecification =
        AuthorSpecification.builder().birthdate(LocalDate.now()).q("test").build();

    List<AuthorResponseDto> result = authorServiceImpl.findAll(authorSpecification);
    assertNotNull(result);
  }

  @Test
  void shouldRegisterAuthor() throws IOException {
    Author author =
        TestUtil.loadObjectFromResource("authors/entity/author_entity.json", Author.class);
    when(authorRepository.save(any(Author.class))).thenReturn(author);
    AuthorProjection authorProjection = TestUtil.getAuthorProjection();
    when(authorRepository.findByUserId(anyInt())).thenReturn(authorProjection);
    RegisterAuthorDto registerAuthorDto =
        TestUtil.loadObjectFromResource(
            "authors/request/author_register.json", RegisterAuthorDto.class);

    AuthorResponseDto result = authorServiceImpl.saveAuthor(registerAuthorDto);
    assertNotNull(result);
  }

  @Test
  void shouldReturnThrowInSave() throws IOException {

    when(authorRepository.save(any(Author.class))).thenThrow(new IllegalArgumentException("test"));
    RegisterAuthorDto registerAuthorDto =
        TestUtil.loadObjectFromResource(
            "authors/request/author_register.json", RegisterAuthorDto.class);

    assertThrows(Exception.class, () -> authorServiceImpl.saveAuthor(registerAuthorDto));
  }

  @Test
  void shouldUpdate() throws IOException {
    Author author =
        TestUtil.loadObjectFromResource("authors/entity/author_entity.json", Author.class);
    when(authorRepository.save(any(Author.class))).thenReturn(author);

    AuthorProjection authorProjection = TestUtil.getAuthorProjection();
    when(authorRepository.findByUserId(anyInt())).thenReturn(authorProjection);
    UpdateAuthorDto updateAuthorDto =
        TestUtil.loadObjectFromResource(
            "authors/request/author_update.json", UpdateAuthorDto.class);

    AuthorResponseDto result = authorServiceImpl.updateAuthor(1, updateAuthorDto);
    assertNotNull(result);
  }

  @Test
  void shouldReturnThrowInUpdate() throws IOException {

    when(authorRepository.save(any(Author.class))).thenThrow(new IllegalArgumentException("test"));

    UpdateAuthorDto updateAuthorDto =
        TestUtil.loadObjectFromResource(
            "authors/request/author_update.json", UpdateAuthorDto.class);

    assertThrows(Exception.class, () -> authorServiceImpl.updateAuthor(1, updateAuthorDto));
  }
}
