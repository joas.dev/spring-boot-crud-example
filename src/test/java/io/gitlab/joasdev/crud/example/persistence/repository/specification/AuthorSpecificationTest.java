package io.gitlab.joasdev.crud.example.persistence.repository.specification;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AuthorSpecificationTest {

  @Mock private Root<Author> root;
  @Mock private CriteriaQuery<?> query;
  @Mock private CriteriaBuilder criteriaBuilder;

  /*
   * @BeforeEach void setupThis() { //ReflectionTestUtils.setField(q, "q",
   * "test"); //ReflectionTestUtils.setField(birthdate, "birthdate",
   * LocalDate.now()); }
   */

  @Test
  void shouldReturnNull() {

    AuthorSpecification authorSpecification = AuthorSpecification.builder().build();

    Predicate result = authorSpecification.toPredicate(root, query, criteriaBuilder);
    assertNull(result);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  void shouldReturnPredicateInOrValue() {

    AuthorSpecification authorSpecification = AuthorSpecification.builder().q("test").build();

    Path<String> firstNamePath = mock(Path.class);
    Path<String> lastNamePath = mock(Path.class);

    Predicate predicate = mock(Predicate.class);
    when(root.get("firstName")).thenReturn((Path) firstNamePath);
    when(root.get("lastName")).thenReturn((Path) lastNamePath);
    when(criteriaBuilder.like(firstNamePath, "%test%")).thenReturn(predicate);
    when(criteriaBuilder.like(lastNamePath, "%test%")).thenReturn(predicate);

    when(criteriaBuilder.or(any(Predicate[].class))).thenReturn(predicate);

    Predicate result = authorSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(criteriaBuilder).like(firstNamePath, "%test%");
    verify(criteriaBuilder).like(lastNamePath, "%test%");
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  void shouldReturnPredicateInAndValue() {

    AuthorSpecification authorSpecification =
        AuthorSpecification.builder().birthdate(LocalDate.now()).build();

    Path<LocalDate> birthdatePath = mock(Path.class);

    Predicate predicate = mock(Predicate.class);
    when(root.get("birthdate")).thenReturn((Path) birthdatePath);
    when(criteriaBuilder.equal(birthdatePath, authorSpecification.getBirthdate()))
        .thenReturn(predicate);

    when(criteriaBuilder.and(any(Predicate[].class))).thenReturn(predicate);

    Predicate result = authorSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(root, times(1)).get("birthdate");
    verify(criteriaBuilder, times(1)).equal(birthdatePath, authorSpecification.getBirthdate());
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  void shouldReturnPredicate() {

    AuthorSpecification authorSpecification =
        AuthorSpecification.builder().q("test").birthdate(LocalDate.now()).build();

    Predicate firstNamePredicate = mock(Predicate.class);
    Predicate lastNamePredicate = mock(Predicate.class);
    Predicate birthdatePredicate = mock(Predicate.class);

    Path<String> firstNamePath = mock(Path.class);
    Path<String> lastNamePath = mock(Path.class);

    when(root.get("firstName")).thenReturn((Path) firstNamePath);
    when(root.get("lastName")).thenReturn((Path) lastNamePath);
    when(criteriaBuilder.like(firstNamePath, "%" + authorSpecification.getQ() + "%"))
        .thenReturn(firstNamePredicate);
    when(criteriaBuilder.like(lastNamePath, "%" + authorSpecification.getQ() + "%"))
        .thenReturn(lastNamePredicate);

    Path<LocalDate> birthdatePath = mock(Path.class);

    when(root.get("birthdate")).thenReturn((Path) birthdatePath);

    when(criteriaBuilder.equal(birthdatePath, authorSpecification.getBirthdate()))
        .thenReturn(birthdatePredicate);

    Predicate predicateAnd = mock(Predicate.class);
    Predicate predicateOr = mock(Predicate.class);
    Predicate predicate = mock(Predicate.class);

    when(criteriaBuilder.or(any(Predicate[].class))).thenReturn(predicateAnd);
    when(criteriaBuilder.and(any(Predicate[].class))).thenReturn(predicateOr);

    // when(criteriaBuilder.or(predicateOr)).thenReturn(predicateCriteriaOr);
    // when(criteriaBuilder.and(predicateAnd,
    // criteriaBuilder.or(predicateOr))).thenReturn(predicate);
    when(criteriaBuilder.and(any(Predicate.class), any(Predicate.class))).thenReturn(predicate);

    Predicate result = authorSpecification.toPredicate(root, query, criteriaBuilder);
    assertNotNull(result);

    verify(root).get("firstName");
    verify(root).get("lastName");
    verify(root, times(1)).get("birthdate");
    verify(criteriaBuilder, times(1)).equal(birthdatePath, authorSpecification.getBirthdate());
    // verify(criteriaBuilder, times(1)).or(any(Predicate[].class));
    // verify(criteriaBuilder, times(1)).and(any(Predicate[].class));
    // verify(criteriaBuilder,
    // times(1)).and(any(Predicate.class),any(Predicate.class));
  }
}
