package io.gitlab.joasdev.crud.example.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.model.vo.AuthorVo;
import io.gitlab.joasdev.crud.example.model.vo.BookVo;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class TestUtil {

  private static ObjectMapper mapper = new ObjectMapper(); // createObjectMapper();

  static {
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.registerModule(new JavaTimeModule());

    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  private TestUtil() {}

  /*
   * private static ObjectMapper createObjectMapper() { ObjectMapper objectMapper
   * = new ObjectMapper();
   * objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
   * false); return objectMapper; }
   */

  public static String toJson(Object object) {
    try {
      return mapper.writeValueAsString(object);
    } catch (Exception e) {
      throw new RuntimeException("Error serializing object to JSON", e);
    }
  }

  public static <T> T loadObjectFromResource(String resourcePath, Class<T> valueType)
      throws IOException {
    try (InputStream inputStream =
        TestUtil.class.getClassLoader().getResourceAsStream(resourcePath)) {
      if (inputStream == null) {
        throw new IllegalArgumentException("The resource was not found: " + resourcePath);
      }
      return mapper.readValue(inputStream, valueType);
    }
  }

  public static <T> T loadObjectFromResource(String resourcePath, TypeReference<T> typeReference) {
    try (InputStream inputStream =
        Objects.requireNonNull(TestUtil.class.getClassLoader().getResourceAsStream(resourcePath))) {
      return mapper.readValue(inputStream, typeReference);
    } catch (Exception e) {
      throw new RuntimeException("Error loading object from resource: " + resourcePath, e);
    }
  }

  /*
   * public static <T> Page<T> loadPageFromResource(String resourcePath, Class<T>
   * elementType) { try (InputStream inputStream =
   * Objects.requireNonNull(TestUtil.class.getClassLoader().getResourceAsStream(
   * resourcePath))) { SimpleModule module = new SimpleModule();
   * mapper.registerModule(module); module.addAbstractTypeMapping(Page.class,
   * PageImpl.class); mapper.registerModule(module);
   *
   * TypeFactory typeFactory = mapper.getTypeFactory(); return
   * mapper.readValue(inputStream,
   * typeFactory.constructParametricType(PageImpl.class,
   * typeFactory.constructType(elementType))); } catch (Exception e) {
   * log.error(e.getMessage()); throw new
   * RuntimeException("Error loading object from resource: " + resourcePath, e); }
   * }
   */

  public static AuthorProjection getAuthorProjection() {
    return AuthorVo.builder()
        .authorId(1)
        .birthdate(LocalDate.now())
        .firtName("firtName")
        .lastName("lastName")
        .fullName("fullName")
        .build();
  }

  public static BookProjection getBookProjection() {
    return BookVo.builder()
        .bookId(1)
        .onlineAvailability(true)
        .publicationDate(LocalDate.now())
        .title("title")
        .build();
  }

  public static List<BookProjection> getBookProjectionList() {
    BookProjection bookProjection = getBookProjection();
    return Arrays.asList(bookProjection);
  }
}
