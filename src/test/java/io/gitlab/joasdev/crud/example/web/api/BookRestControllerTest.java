package io.gitlab.joasdev.crud.example.web.api;

import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.gitlab.joasdev.crud.example.SpringBootCrudExampleApplication;
import io.gitlab.joasdev.crud.example.model.dto.BookItemDto;
import io.gitlab.joasdev.crud.example.model.dto.BookResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateBookDto;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.BookSpecification;
import io.gitlab.joasdev.crud.example.service.BookService;
import io.gitlab.joasdev.crud.example.util.PageUtil;
import io.gitlab.joasdev.crud.example.util.TestUtil;
import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = BookRestController.class)
@ContextConfiguration(classes = {SpringBootCrudExampleApplication.class})
class BookRestControllerTest {

  @MockitoBean private BookService bookService;

  @MockitoBean private DataSource dataSource;

  @Autowired private MockMvc mockMvc;

  @Test
  void shouldRegisterBook() throws Exception {

    RegisterBookDto registerBookDto =
        TestUtil.loadObjectFromResource("books/request/register_book.json", RegisterBookDto.class);
    BookResponseDto bookResponseDto =
        TestUtil.loadObjectFromResource("books/response/book_response.json", BookResponseDto.class);
    when(bookService.saveBook(any(RegisterBookDto.class))).thenReturn(bookResponseDto);

    this.mockMvc
        .perform(
            post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJson(registerBookDto)))
        .andDo(print())
        .andExpect(status().isCreated());

    verify(bookService, times(1)).saveBook(any(RegisterBookDto.class));
  }

  @Test
  void shouldUpdateBook() throws Exception {

    UpdateBookDto updateBookDto =
        TestUtil.loadObjectFromResource(
            "books/request/update_book_request.json", UpdateBookDto.class);
    BookResponseDto bookResponseDto =
        TestUtil.loadObjectFromResource("books/response/book_response.json", BookResponseDto.class);
    when(bookService.updateBook(anyInt(), any(UpdateBookDto.class))).thenReturn(bookResponseDto);

    this.mockMvc
        .perform(
            put("/books/{bookId}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJson(updateBookDto)))
        .andDo(print())
        .andExpect(status().isOk());

    verify(bookService, times(1)).updateBook(anyInt(), any(UpdateBookDto.class));
  }

  @Test
  void shouldDeleteBook() throws Exception {

    doNothing().when(bookService).deleteBook(anyInt());

    this.mockMvc
        .perform(delete("/books/{bookId}", 1).contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isNoContent());

    verify(bookService, times(1)).deleteBook(anyInt());
  }

  @Test
  void shouldReturnBookById() throws Exception {

    BookResponseDto bookResponseDto =
        TestUtil.loadObjectFromResource("books/response/book_response.json", BookResponseDto.class);
    when(bookService.findBookId(anyInt())).thenReturn(bookResponseDto);

    this.mockMvc
        .perform(get("/books/{bookId}", 1).contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());

    verify(bookService, times(1)).findBookId(anyInt());
  }

  @Test
  void shouldReturnList() throws Exception {

    try (MockedStatic<PageUtil> pageUtilMock = mockStatic(PageUtil.class)) {

      Sort sort = Sort.by("test").ascending();
      pageUtilMock.when(() -> PageUtil.getSort(any())).thenReturn(sort);

      BookResponseDto bookResponseDto =
          TestUtil.loadObjectFromResource(
              "books/response/book_response.json", BookResponseDto.class);
      List<BookResponseDto> bookItemDtoList = Arrays.asList(bookResponseDto);

      when(bookService.findAllToList(any(BookSpecification.class), any(Sort.class)))
          .thenReturn(bookItemDtoList);

      this.mockMvc
          .perform(
              get("/books/list")
                  .queryParam("q", "")
                  // .queryParam("publicationDate", "2025/01/01")
                  .queryParam("onlineAvailability", "true")
                  .queryParam("page", "0")
                  .queryParam("size", "10")
                  .queryParam("sort", "test.asc")
                  .contentType(MediaType.APPLICATION_JSON))
          .andDo(print())
          .andExpect(status().isOk());

      verify(bookService, times(1)).findAllToList(any(BookSpecification.class), any(Sort.class));

    } catch (Exception e) {
      fail("error");
    }
  }

  @Test
  void shouldReturnPage() throws Exception {

    try (MockedStatic<PageUtil> pageUtilMock = mockStatic(PageUtil.class)) {

      Sort sort = Sort.by("test").ascending();
      pageUtilMock.when(() -> PageUtil.getSort(any())).thenReturn(sort);

      BookItemDto bookItemDto =
          TestUtil.loadObjectFromResource("books/response/book_response.json", BookItemDto.class);
      List<BookItemDto> bookItemDtoList = Arrays.asList(bookItemDto);
      Pageable pageable = PageRequest.of(0, 10, sort);
      Page<BookItemDto> page = new PageImpl<>(bookItemDtoList, pageable, bookItemDtoList.size());

      when(bookService.findAllToPage(any(BookSpecification.class), any(Pageable.class)))
          .thenReturn(page);

      this.mockMvc
          .perform(
              get("/books/pages")
                  .queryParam("q", "")
                  // .queryParam("publicationDate", "2025/01/01")
                  .queryParam("onlineAvailability", "true")
                  .queryParam("page", "0")
                  .queryParam("size", "10")
                  .queryParam("sort", "test.asc")
                  .contentType(MediaType.APPLICATION_JSON))
          .andDo(print())
          .andExpect(status().isOk());

      verify(bookService, times(1))
          .findAllToPage(any(BookSpecification.class), any(Pageable.class));

    } catch (Exception e) {
      fail("error");
    }
  }
}
