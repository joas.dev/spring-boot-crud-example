package io.gitlab.joasdev.crud.example.web.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.gitlab.joasdev.crud.example.SpringBootCrudExampleApplication;
import io.gitlab.joasdev.crud.example.model.dto.AuthorItemDto;
import io.gitlab.joasdev.crud.example.model.dto.AuthorResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.AuthorSpecification;
import io.gitlab.joasdev.crud.example.service.AuthorService;
import io.gitlab.joasdev.crud.example.util.TestUtil;
import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AuthorRestController.class)
@ContextConfiguration(classes = {SpringBootCrudExampleApplication.class})
class AuthorRestControllerTest {

  @MockitoBean private AuthorService authorService;
  @MockitoBean private DataSource dataSource;

  @Autowired private MockMvc mockMvc;

  @Test
  void shouldRemoved() throws Exception {
    doNothing().when(authorService).deleteAuthor(anyInt());

    this.mockMvc
        .perform(delete("/authors/{authorId}", 1))
        .andDo(print())
        .andExpect(status().isNoContent());
  }

  @Test
  void shouldRegisterANewAuthor() throws Exception {

    AuthorResponseDto authorResponseDto =
        TestUtil.loadObjectFromResource("authors/response/author.json", AuthorResponseDto.class);

    when(authorService.saveAuthor(any(RegisterAuthorDto.class))).thenReturn(authorResponseDto);
    RegisterAuthorDto registerAuthorDto = new RegisterAuthorDto();
    registerAuthorDto.setFirstName("firstName");
    registerAuthorDto.setLastName("lastName");
    registerAuthorDto.setBirthdate("03/01/2025");

    this.mockMvc
        .perform(
            post("/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJson(registerAuthorDto)))
        .andDo(print())
        .andExpect(status().isCreated());

    verify(authorService, times(1)).saveAuthor(registerAuthorDto);
  }

  @Test
  @DisplayName("Should update the author data")
  void shouldUpdate() throws Exception {
    AuthorResponseDto authorResponseDto = new AuthorResponseDto();
    authorResponseDto.setAuthorId(1);
    authorResponseDto.setFirstName("firstName");
    authorResponseDto.setLastName("lastName");
    authorResponseDto.setFullName("firstName lastName");
    authorResponseDto.setBirthdate("1981/10/10");

    when(authorService.updateAuthor(anyInt(), any(UpdateAuthorDto.class)))
        .thenReturn(authorResponseDto);

    UpdateAuthorDto updateAuthorDto = new UpdateAuthorDto();
    updateAuthorDto.setFirstName("firstName");
    updateAuthorDto.setLastName("lastName");
    updateAuthorDto.setBirthdate("03/01/2025");

    this.mockMvc
        .perform(
            put("/authors/{authorId}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJson(updateAuthorDto)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content.authorId").value(1));
  }

  @Test
  void shouldReturnAuthorResponseDto() throws Exception {
    AuthorResponseDto authorResponseDto = new AuthorResponseDto();
    authorResponseDto.setAuthorId(1);
    authorResponseDto.setFirstName("firstName");
    authorResponseDto.setLastName("lastName");
    authorResponseDto.setFullName("firstName lastName");
    authorResponseDto.setBirthdate("1981/10/10");

    when(authorService.findAuthorId(anyInt())).thenReturn(authorResponseDto);

    UpdateAuthorDto updateAuthorDto = new UpdateAuthorDto();
    updateAuthorDto.setFirstName("firstName");
    updateAuthorDto.setLastName("lastName");
    updateAuthorDto.setBirthdate("03/01/2025");

    this.mockMvc
        .perform(get("/authors/{authorId}", 1).contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.authorId").value(1));
  }

  @Test
  void shouldReturnList() throws Exception {
    AuthorResponseDto authorResponseDto = new AuthorResponseDto();
    authorResponseDto.setAuthorId(1);
    authorResponseDto.setFirstName("firstName");
    authorResponseDto.setLastName("lastName");
    authorResponseDto.setFullName("firstName lastName");
    authorResponseDto.setBirthdate("1981/10/10");

    List<AuthorResponseDto> authorResponseDtoList = Arrays.asList(authorResponseDto);

    when(authorService.findAll(any(AuthorSpecification.class))).thenReturn(authorResponseDtoList);

    UpdateAuthorDto updateAuthorDto = new UpdateAuthorDto();
    updateAuthorDto.setFirstName("firstName");
    updateAuthorDto.setLastName("lastName");
    updateAuthorDto.setBirthdate("03/01/2025");

    this.mockMvc
        .perform(get("/authors/list").contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[0].authorId").value(1));
  }

  @Test
  void shouldReturnPage() throws Exception {
    AuthorItemDto authorItemDto = new AuthorItemDto();
    authorItemDto.setAuthorId(1);
    authorItemDto.setFirstName("firstName");
    authorItemDto.setLastName("lastName");
    authorItemDto.setBirthdate("1981/10/10");

    List<AuthorItemDto> authorItemDtoList = Arrays.asList(authorItemDto);
    Pageable pageable = PageRequest.of(0, 10);
    Page<AuthorItemDto> authorResponseDtoPage =
        new PageImpl<>(authorItemDtoList, pageable, authorItemDtoList.size());
    when(authorService.findAllToPage(any(AuthorSpecification.class), any(Pageable.class)))
        .thenReturn(authorResponseDtoPage);

    UpdateAuthorDto updateAuthorDto = new UpdateAuthorDto();
    updateAuthorDto.setFirstName("firstName");
    updateAuthorDto.setLastName("lastName");
    updateAuthorDto.setBirthdate("03/01/2025");

    this.mockMvc
        .perform(
            get("/authors/pages")
                .param("q", "q")
                .param("sort", "test,asc,test2,desc")
                .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content.[0].authorId").value(1));
  }

  @Test
  void shouldReturnThrowInPage() throws Exception {

    this.mockMvc
        .perform(get("/authors/pages").param("q", "q").param("sort", "test"))
        .andDo(print())
        .andExpect(status().isBadRequest());
  }
}
