package io.gitlab.joasdev.crud.example.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.BookItemDto;
import io.gitlab.joasdev.crud.example.model.dto.BookResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateBookDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import io.gitlab.joasdev.crud.example.persistence.entity.Book;
import io.gitlab.joasdev.crud.example.persistence.entity.BookAuthor;
import io.gitlab.joasdev.crud.example.persistence.entity.BookAuthorId;
import io.gitlab.joasdev.crud.example.persistence.repository.BookAuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.BookRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.BookSpecification;
import io.gitlab.joasdev.crud.example.service.impl.BookServiceImpl;
import io.gitlab.joasdev.crud.example.util.TestUtil;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Slf4j
@ExtendWith(MockitoExtension.class)
class BookServiceTest {

  @InjectMocks private BookServiceImpl bookService;

  @Mock private BookRepository bookRepository;
  @Mock private BookAuthorRepository bookAuthorRepository;

  @Test
  void shouldCreateBook() {

    Book book = new Book();
    book.setBookId(1);
    book.setTitle("title");
    book.setOnlineAvailability(true);
    book.setPublicationDate(LocalDate.now());

    when(bookRepository.save(any(Book.class))).thenReturn(book);

    Author authorId = new Author();
    authorId.setAuthorId(1);
    authorId.setBirthdate(LocalDate.now());
    authorId.setFirstName("firstName");
    authorId.setLastName("lastName");

    Book bookId = new Book();
    bookId.setBookId(1);
    bookId.setOnlineAvailability(true);
    bookId.setPublicationDate(LocalDate.now());
    bookId.setTitle("title");

    BookAuthorId bookAuthorId = new BookAuthorId();
    bookAuthorId.setAuthorId(authorId);
    bookAuthorId.setBookId(bookId);

    BookAuthor bookAuthor = new BookAuthor();
    bookAuthor.setBookAuthorId(bookAuthorId);

    when(bookAuthorRepository.save(any(BookAuthor.class))).thenReturn(bookAuthor);

    BookProjection bookProjection = mock(BookProjection.class);
    when(bookProjection.getTitle()).thenReturn("title");
    when(bookProjection.getBookId()).thenReturn(1);

    Optional<BookProjection> bookPojectionOp = Optional.of(bookProjection);
    log.info(bookPojectionOp.get().getTitle());
    when(bookAuthorRepository.findByBookId(anyInt())).thenReturn(bookPojectionOp);

    List<Integer> authors = Arrays.asList(1, 2, 3);

    RegisterBookDto registerBookDto = new RegisterBookDto();
    registerBookDto.setTitle("title");
    registerBookDto.setOnlineAvailability(true);
    registerBookDto.setAuthors(authors);

    BookResponseDto result = bookService.saveBook(registerBookDto);
    assertNotNull(result);

    verify(bookRepository, times(1)).save(any(Book.class));
    verify(bookAuthorRepository, times(3)).save(any(BookAuthor.class));
    verify(bookAuthorRepository, times(1)).findByBookId(anyInt());
  }

  @Test
  void shouldReturnThrowInSaveMethod() {

    when(bookRepository.save(any(Book.class))).thenThrow(new IllegalArgumentException("test"));

    List<Integer> authors = Arrays.asList(1, 2, 3);
    RegisterBookDto registerBookDto = new RegisterBookDto();
    registerBookDto.setTitle("title");
    registerBookDto.setOnlineAvailability(true);
    registerBookDto.setAuthors(authors);

    assertThrows(ApiException.class, () -> bookService.saveBook(registerBookDto));
  }

  @Test
  void shouldDeleteBook() {

    when(bookAuthorRepository.deleteByBookId(anyInt())).thenReturn(1);
    doNothing().when(bookRepository).delete(any(Book.class));

    bookService.deleteBook(1);

    verify(bookAuthorRepository, times(1)).deleteByBookId(anyInt());
  }

  @Test
  void shouldUpdateBook() throws IOException {

    when(bookAuthorRepository.deleteByBookId(anyInt())).thenReturn(1);
    Book book = TestUtil.loadObjectFromResource("books/response/book.json", Book.class);
    when(bookRepository.save(any(Book.class))).thenReturn(book);
    BookAuthor bookAuthor =
        TestUtil.loadObjectFromResource("books/response/book_author.json", BookAuthor.class);
    List<BookAuthor> bookAuthors = Arrays.asList(bookAuthor);
    when(bookAuthorRepository.saveAll(anyList())).thenReturn(bookAuthors);

    BookProjection bookProjection = mock(BookProjection.class);
    when(bookProjection.getTitle()).thenReturn("title");
    when(bookProjection.getBookId()).thenReturn(1);

    Optional<BookProjection> bookPojectionOp = Optional.of(bookProjection);
    when(bookAuthorRepository.findByBookId(anyInt())).thenReturn(bookPojectionOp);

    UpdateBookDto updateBookDto =
        TestUtil.loadObjectFromResource(
            "books/request/update_book_request.json", UpdateBookDto.class);

    BookResponseDto result = bookService.updateBook(1, updateBookDto);
    assertNotNull(result);
  }

  @Test
  void shouldReturnList() {

    List<BookProjection> bookProjections = TestUtil.getBookProjectionList();
    when(bookRepository.findAllToList(any(BookSpecification.class), any(Sort.class)))
        .thenReturn(bookProjections);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .onlineAvailability(true)
            .q("q")
            .publicationDate(LocalDate.now())
            .build();
    Sort sort = Sort.by("test").ascending();

    List<BookResponseDto> result = bookService.findAllToList(bookSpecification, sort);
    assertNotNull(result);
  }

  @Test
  void shouldReturnPage() throws IOException {
    List<BookProjection> bookProjectionList = TestUtil.getBookProjectionList();

    Page<BookProjection> bookProjectionPage = new PageImpl<>(bookProjectionList);
    when(bookRepository.findAllToPage(any(BookSpecification.class), any(Pageable.class)))
        .thenReturn(bookProjectionPage);

    BookSpecification bookSpecification =
        BookSpecification.builder()
            .onlineAvailability(true)
            .q("q")
            .publicationDate(LocalDate.now())
            .build();
    Sort sort = Sort.by("test").ascending();
    Pageable pageable = PageRequest.of(0, 10, sort);

    Page<BookItemDto> result = bookService.findAllToPage(bookSpecification, pageable);
    assertNotNull(result);
  }

  @Test
  void shouldReturnThrowInDeleteBookMethod() {
    when(bookAuthorRepository.deleteByBookId(anyInt())).thenReturn(1);
    doThrow(new IllegalArgumentException("test")).when(bookRepository).delete(any(Book.class));

    assertThrows(ApiException.class, () -> bookService.deleteBook(1));
  }

  @Test
  void shouldReturnThrowInFindBookIdMethod() {
    when(bookAuthorRepository.findByBookId(anyInt())).thenReturn(Optional.empty());

    assertThrows(ApiException.class, () -> bookService.findBookId(1));
  }

  @Test
  void shouldReturnThrowInUpdateBookMethod() throws IOException {

    when(bookAuthorRepository.deleteByBookId(anyInt())).thenReturn(1);
    when(bookRepository.save(any(Book.class))).thenThrow(new IllegalArgumentException("test"));

    UpdateBookDto updateBookDto =
        TestUtil.loadObjectFromResource(
            "books/request/update_book_request.json", UpdateBookDto.class);

    assertThrows(ApiException.class, () -> bookService.updateBook(1, updateBookDto));
  }
}
