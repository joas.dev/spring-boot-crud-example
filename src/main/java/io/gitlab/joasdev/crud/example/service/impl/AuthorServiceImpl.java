package io.gitlab.joasdev.crud.example.service.impl;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.mapper.AuthorMapper;
import io.gitlab.joasdev.crud.example.model.dto.AuthorItemDto;
import io.gitlab.joasdev.crud.example.model.dto.AuthorResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import io.gitlab.joasdev.crud.example.persistence.repository.AuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.BookAuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.AuthorSpecification;
import io.gitlab.joasdev.crud.example.service.AuthorService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

  private final AuthorRepository authorRepository;
  private final BookAuthorRepository bookAuthorRepository;

  @Override
  @Transactional
  public AuthorResponseDto saveAuthor(RegisterAuthorDto registerAuthorDto) {

    Author authorEntity = null;

    Author authorResult = null;
    try {
      authorEntity = AuthorMapper.INSTANCE.registerAuthorDtoToAuthor(registerAuthorDto);
      authorResult = authorRepository.save(authorEntity);
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage());
      throw new ApiException("Error al insetar datos", HttpStatus.BAD_REQUEST);
    }

    return findAuthorId(authorResult.getAuthorId());
  }

  @Override
  public Page<AuthorItemDto> findAllToPage(
      AuthorSpecification authorSpecification, Pageable pageable) {

    Page<Author> result = null;
    try {
      result = authorRepository.findAll(authorSpecification, pageable);
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new ApiException("Empty", HttpStatus.NOT_FOUND);
    }

    List<AuthorItemDto> authorItemDtos =
        AuthorMapper.INSTANCE.authorToAuthorItemDto(result.getContent());

    return new PageImpl<>(authorItemDtos, result.getPageable(), result.getTotalElements());
  }

  @Override
  @Transactional
  public void deleteAuthor(Integer authorId) {

    try {

      Boolean exist = bookAuthorRepository.existByAuthorId(authorId);

      if (exist != null && exist) {
        bookAuthorRepository.deleteByAuthorId(authorId);
      }

      authorRepository.delete(authorId);
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new ApiException("Empty", HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public AuthorResponseDto findAuthorId(Integer authorId) throws ApiException {

    AuthorProjection authorProjection = null;

    try {
      authorProjection = authorRepository.findByUserId(authorId);
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new ApiException("Empty", HttpStatus.NOT_FOUND);
    }

    if (authorProjection == null) {
      throw new ApiException("Not exist author " + authorId, HttpStatus.NOT_FOUND);
    }

    return AuthorMapper.INSTANCE.authorProjectionToAuthorResponseDto(authorProjection);
  }

  @Override
  @Transactional
  public AuthorResponseDto updateAuthor(Integer authorId, UpdateAuthorDto updateAuthorDto) {

    Author authorEntity = null;
    Author authorResult = null;
    try {
      authorEntity = AuthorMapper.INSTANCE.updateAuthorDtoToAuthor(updateAuthorDto);
      authorEntity.setAuthorId(authorId);
      authorResult = authorRepository.save(authorEntity);
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage());
      throw new ApiException("Error in Update " + authorId, HttpStatus.NOT_FOUND);
    }

    AuthorProjection authorProjection = authorRepository.findByUserId(authorResult.getAuthorId());
    return AuthorMapper.INSTANCE.authorProjectionToAuthorResponseDto(authorProjection);
  }

  @Override
  public List<AuthorResponseDto> findAll(AuthorSpecification authorSpecification) {
    List<Author> authors = authorRepository.findAll(authorSpecification);

    return AuthorMapper.INSTANCE.authorToAuthorResponseDto(authors);
  }
}
