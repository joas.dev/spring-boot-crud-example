package io.gitlab.joasdev.crud.example.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ApiException extends RuntimeException {

  private static final long serialVersionUID = 3980287382237745392L;

  private final String message;
  private final HttpStatus httpStatus;

  public ApiException(String message, HttpStatus httpStatus) {
    super(message);
    this.message = message;
    this.httpStatus = httpStatus;
  }

  @Override
  public String toString() {
    return message;
  }
}
