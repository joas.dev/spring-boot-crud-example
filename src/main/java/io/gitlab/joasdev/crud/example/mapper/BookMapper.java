package io.gitlab.joasdev.crud.example.mapper;

import io.gitlab.joasdev.crud.example.model.dto.BookItemDto;
import io.gitlab.joasdev.crud.example.model.dto.BookResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateBookDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Book;
import io.gitlab.joasdev.crud.example.util.DateTimeUtil;
import java.time.LocalDate;
import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BookMapper {

  public static BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

  @Mapping(
      source = "publicationDate",
      target = "publicationDate",
      qualifiedByName = "stringToLocalDate")
  Book registerBookDtoToBook(RegisterBookDto registerBookDto);

  @Named("stringToLocalDate")
  default LocalDate stringToLocalDate(String dateAsString) {
    return DateTimeUtil.stringToLocalDate(dateAsString, DateTimeUtil.DAY_MONTH_YEAR);
  }

  @Mapping(source = "publicationDate", target = "publicationDate", dateFormat = "yyyy/MM/dd")
  BookItemDto bookProjectionToBookItemDto(BookProjection bookProjection);

  List<BookItemDto> bookProjectionListToBookItemDtoList(List<BookProjection> content);

  BookResponseDto bookProjectionToBookResponseDto(BookProjection bookProjection);

  @Mapping(
      source = "publicationDate",
      target = "publicationDate",
      qualifiedByName = "stringToLocalDate")
  Book updateBookDtoToBook(UpdateBookDto updateBookDto);

  @Named("mapToBookResponseDto")
  @Mapping(source = "publicationDate", target = "publicationDate", dateFormat = "yyyy/MM/dd")
  BookResponseDto bookResponseDtoToBookProjection(BookProjection book);

  @IterableMapping(qualifiedByName = "mapToBookResponseDto")
  List<BookResponseDto> bookProjectionListToBookResponseDtoList(List<BookProjection> books);
}
