package io.gitlab.joasdev.crud.example.persistence.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "authors")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Author implements Serializable {

  private static final long serialVersionUID = 2520773500635676241L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer authorId;

  private String firstName;
  private String lastName;

  @JsonFormat(pattern = "dd/MM/yyyy")
  private LocalDate birthdate;
}
