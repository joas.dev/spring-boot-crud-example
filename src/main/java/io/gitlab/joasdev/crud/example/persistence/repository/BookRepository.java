package io.gitlab.joasdev.crud.example.persistence.repository;

import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.dao.BookDao;
import io.gitlab.joasdev.crud.example.persistence.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BookRepository
    extends JpaRepository<Book, Integer>,
        // CrudRepository<Book, Integer>,
        // PagingAndSortingRepository<Book, Integer>,
        JpaSpecificationExecutor<BookProjection>,
        BookDao {

  /*
   * @Modifying
   *
   * @Query("INSERT INTO Book (title,publicationDate) VALUES (:#{#book.title},
   * :#{#book.publicationDate})" ) Integer save(@Param("book") Book book);
   *
   *
   *
   * @Modifying
   *
   * @Query("DELETE FROM Book b WHERE b.bookId = :bookId ") void
   * delete(@Param("bookId") Integer bookId);
   */
  // @Query("SELECT b.title, b.publicationDate FROM Book b WHERE b.bookId =
  // :bookId")
  // Optional<BookProjection> findByBookId(@Param("bookId") Integer bookId);
}
