package io.gitlab.joasdev.crud.example.persistence.dao;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.BookSpecification;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface BookDao {

  Page<BookProjection> findAllToPage(BookSpecification bookSpec, Pageable pageable)
      throws ApiException;

  List<BookProjection> findAllToList(BookSpecification bookSpec, Sort sort);
}
