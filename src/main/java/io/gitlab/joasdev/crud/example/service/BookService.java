package io.gitlab.joasdev.crud.example.service;

import io.gitlab.joasdev.crud.example.model.dto.BookItemDto;
import io.gitlab.joasdev.crud.example.model.dto.BookResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateBookDto;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.BookSpecification;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface BookService {

  BookResponseDto saveBook(RegisterBookDto registerBookDto);

  Page<BookItemDto> findAllToPage(BookSpecification bookSpec, Pageable pageable);

  BookResponseDto findBookId(Integer bookId);

  void deleteBook(Integer bookId);

  BookResponseDto updateBook(Integer bookId, UpdateBookDto updateBookDto);

  List<BookResponseDto> findAllToList(BookSpecification bookSpec, Sort sort);
}
