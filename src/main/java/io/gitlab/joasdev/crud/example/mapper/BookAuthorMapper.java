package io.gitlab.joasdev.crud.example.mapper;

import io.gitlab.joasdev.crud.example.model.dto.BookResponseDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BookAuthorMapper {

  static BookAuthorMapper INSTANCE = Mappers.getMapper(BookAuthorMapper.class);

  BookResponseDto bookProjectionTobookResponseDto(BookProjection bookProjection);
}
