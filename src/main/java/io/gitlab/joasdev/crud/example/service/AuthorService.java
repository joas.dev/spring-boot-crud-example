package io.gitlab.joasdev.crud.example.service;

import io.gitlab.joasdev.crud.example.model.dto.AuthorItemDto;
import io.gitlab.joasdev.crud.example.model.dto.AuthorResponseDto;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.persistence.repository.specification.AuthorSpecification;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuthorService {

  AuthorResponseDto saveAuthor(RegisterAuthorDto registerAuthorDto);

  Page<AuthorItemDto> findAllToPage(AuthorSpecification authorSpecification, Pageable pageable);

  AuthorResponseDto updateAuthor(Integer authorId, UpdateAuthorDto updateAuthorDto);

  void deleteAuthor(Integer authorId);

  AuthorResponseDto findAuthorId(Integer authorId);

  List<AuthorResponseDto> findAll(AuthorSpecification authorSpecification);
}
