# Spring Boot Crud Example
This project gives examples of how to do a CRUD with Spring Boot + Spring Data JPA

[![Crea un CRUD como los mismísimos dioses 🔥🔥🔥](https://i.ytimg.com/vi/ViOi17NAmDA/maxresdefault.jpg)](https://www.youtube.com/live/s7a9aVGxwxs)

- v2.0.0: [https://www.youtube.com/live/ViOi17NAmDA](https://www.youtube.com/live/ViOi17NAmDA)
- v1.0.0: [https://www.youtube.com/live/s7a9aVGxwxs](https://www.youtube.com/live/s7a9aVGxwxs)


## Requirements
- [Java 17](https://adoptium.net/) or higher. Setting the [JAVA_HOME](https://www.baeldung.com/java-home-on-windows-7-8-10-mac-os-x-linux) environment variable
- [Maven](https://maven.apache.org/download.cgi)
- [Lombok](https://projectlombok.org/download)

## Project features

- OpenAPI
- [OpenAPI Generator](https://www.youtube.com/live/5QLgt3RV0mU?si=zgBtxJo2Gv8r1zod) | [Video](https://www.youtube.com/live/5QLgt3RV0mU?si=zgBtxJo2Gv8r1zod)
- Spring Boot
- Spring Data JPA
- [Cors](https://www.baeldung.com/spring-cors)
- [MapStruct](https://mapstruct.org/)
- [Lombok](https://projectlombok.org/)
- Repository
- DAO
- Create your own insert, update, and delete queries
- [Projection](https://www.baeldung.com/spring-data-jpa-projections)
- [Criteria queries](https://www.baeldung.com/spring-data-criteria-queries)
- [JdbcTemplate](https://www.baeldung.com/spring-jdbc-jdbctemplate)
- [Pagination](https://www.baeldung.com/spring-data-jpa-pagination-sorting)
- Specification / Criteria
- [RestControllerAdvice](https://websparrow.org/spring/spring-restcontrolleradvice-annotation-example)
- Generics
- Transactional
- Managing many-to-many relationships
- [H2](https://www.h2database.com/)
- [Testing (JUnit and Mockito)](https://www.youtube.com/live/FhXwx0rb53A?si=Qq2sfTTXUA7yIwc8) | [Video1](https://www.youtube.com/live/FhXwx0rb53A?si=Qq2sfTTXUA7yIwc8) | [Video2](https://www.youtube.com/live/XX2SJ6uiwtQ?si=yrq8DAlNM8tE0QC8)

## Project settings
- [Checkstyle](https://www.baeldung.com/checkstyle-java) | [Video](https://youtu.be/RJMB6ZVT_Fs?si=Xym7iMXdi6xm_-7p)
- [JaCoCo](https://medium.com/@truongbui95/jacoco-code-coverage-with-spring-boot-835af8debc68)
- [Java Maven Spotless](https://www.baeldung.com/java-maven-spotless-plugin)

## Instructions

If you want to run the project and test from consoleIf you want to run the project and test from console

```
mvn clean install
mvn spring-boot:run
```

Use [Swagger Editor](https://editor.swagger.io/) or another service or program to view and run the services defined in [src/main/java/resources/openapi/spring-boot-crud-example.yaml](./src/main/resources/openapi/spring-boot-crud-example.yaml).

## Additional Commands

Jacoco

```
mvn jacoco:report
```

Checkstyle

```
mvn checkstyle:check
```

Run to correct formatting violations.

```
mvn spotless:apply
```

## Social networks
*  [Discord](https://discord.gg/VdEw8UKx5z)
*  [Youtube Developers](https://www.youtube.com/@JoasDev)
*  [Youtube Oficial](https://www.youtube.com/@TetradotoxinaOficial)
*  [Twitch](https://www.twitch.tv/tetradotoxina)
*  [Fanpage Oficial (Entertainment)](https://bit.ly/3wHqavn)
*  [Fanpage Developers (Developers)](https://bit.ly/3upIG9V)
*  [Facebook Group (Developers)](https://bit.ly/3bTZaAQ)
*  [Web](https://tetradotoxina.com)
